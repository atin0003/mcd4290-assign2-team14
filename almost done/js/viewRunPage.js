// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
var parsedIndex = JSON.parse(runIndex);
if (runIndex !== null)
{
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    var runNames = [ "Run A", "Run B" ];
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
}


    var theRun= JSON.parse(localStorage.getItem('key'));

    
        var starting_Location = theRun._startLocation
        var end_Location = theRun._endLocation
        var start_Time = theRun._startTime
        var end_Time = theRun._endTime
    
    
    
    var startLoc = document.getElementById('startLoc')
    startLoc.innerHTML += starting_Location

    var endLoc = document.getElementById('endLoc')
    endLoc.innerHTML += end_Location

    var startTime = document.getElementById('startTime')
    startTime.innerHTML += start_Time

    var disA= end_Location-starting_Location; 
    var disB= end_Location-starting_Location; 
    var distances = Math.sqrt(Math.pow(disA,2)+Math.pow(disB,2));
    
    var distance = document.getElementById('distance')
    distance.innerHTML += distances

    var duration = end_Time - start_Time;
    var durations = document.getElementById('duration')
    durations.innerHTML += duration

    var averageSpeed = distances/duration
    var avg_Speed = document.getElementById('avgSpeed')
    avg_Speed.innerHTML += averageSpeed


function deleteRun()
{
    theRun.splice(parsedIndex,1)
    localStorage.setItem('key',JSON.stringify(theRun))
}

 map.panTo(starting_Location)
 var randomMarker1 = starting_Location
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker1)
                .addTo(map);

    var randomMarker2 = end_Location
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker2)
                .addTo(map);

map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [randomMarker1,randomMarker2]
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 8
        }
    }); 
      