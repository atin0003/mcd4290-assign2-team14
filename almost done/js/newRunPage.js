// Code for the Measure Run page.
var latitude
var longitude
var currentLoc

navigator.geolocation.watchPosition(currentLocation)


function currentLocation(position){
    latitude = position.coords.latitude
    longitude = position.coords.longitude
    currentLoc = [longitude,latitude]
   return currentLoc 
}

navigator.geolocation.getCurrentPosition(currentAccuracy)

function currentAccuracy(pos)
{
   
    var acc = pos.coords.accuracy
    var accc = document.getElementById('curAcc')
    accc.innerHTML += acc
    
    /*if  (acc>20)
    {
        document.getElementById("buttonRD").disabled = true;
        
    }  
    
    else
    {
        document.getElementById("buttonRD").disabled = false;
    }*/
    
}

function randomDestination(array) {
    var destination = ['','']
    var num = (Math.random() * 0.0006) + 0.0004 ;
    var angle = Math.random() * (2*(Math.PI))
    if (angle>(1.5*Math.PI))
    {
        destination[0] = array[0] + num*Math.cos((2*(Math.PI))-angle)
        destination[1] = array[1] - num*Math.sin((2*(Math.PI))-angle)
        
    }
    else if (angle>(Math.PI))
    {
        destination[0] = array[0] - num*Math.cos(angle-(Math.PI))
        destination[1] = array[1] - num*Math.sin(angle-(Math.PI))
    }
    else if (angle>(0.5*Math.PI))
    {
        destination[0] = array[0] - num*Math.cos((Math.PI)-angle)
        destination[1] = array[1] + num*Math.sin((Math.PI)-angle)
    }
    else
    {
        destination[0] = array[0] + num*Math.cos(angle)
        destination[1] = array[1] + num*Math.sin(angle)
    }
   
    
        ;
        
    return destination
}

var markers = [];
function takeRandomDest()
{
   
    var randomMarker = randomDestination(currentLoc)
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker)
                .addTo(map);
    markers.push(marker1)
    
    map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [randomMarker,currentLoc]
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 8
        }
    }); 
      
    
    
    document.getElementById("buttonRD").disabled = true
    document.getElementById("delButtonRD").disabled = false
    document.getElementById("startRD").disabled = false
}

function deleteRandomDest()
{
    for (var i = markers.length - 1; i >= 0; i--) 
    {
      markers[i].remove();
        
    }
    
    document.getElementById("delButtonRD").disabled = true
    document.getElementById("startRD").disabled = true
    document.getElementById("buttonRD").disabled = false
}

function startRun(startLoc,desLoc) 
{   
    var attributes=[]
    attributes.push(startLoc)
    attributes.push(desLoc)
    var startTime=new Date()
    attributes.push(startTime)
    console.log(startTime)
    var disA=desLoc[0]-startLoc[0];
    console.log(disA)
    var disB=desLoc[1]-startLoc[1]; 
    var distance=Math.sqrt(Math.pow(disA,2)+Math.pow(disB,2));
    var roundedDistance = ((distance/0.001)*150).toFixed(1)
    attributes.push(roundedDistance)
    var dist = document.getElementById('curDis')
    dist.innerHTML += roundedDistance
    
    
    document.getElementById("delButtonRD").disabled = true;
    document.getElementById("storeBut").disabled = false;
    document.getElementById("startRD").disabled = true;
    document.getElementById("buttonRD").disabled = true;
    console.log(attributes)
    return attributes
} 

function stopRun()
{
    var stopTime=new Date()
    return stopTime
}
var runList=[]

function storeRun()
{
    var stopTime=stopRun()
    var runAttributes=startRun(currentLoc,randomDestination(currentLoc))
    console.log(runAttributes)
    var timeStart=runAttributes[2]
    var startLoc=runAttributes[0]
    var desLoc=runAttributes[1]
    var distance=runAttributes[3]
    var run=new Run(startLoc,desLoc,timeStart,stopTime,distance)
    console.log(run)
    runList.push(run)
    console.log(runList)
    document.getElementById("storeBut").disabled = false;
    document.getElementById("stopRD").disabled = true;
    document.getElementById("buttonRD").disabled = false;
    return runList
}





var key='runs'
function localStore(object)
{
    localStorage.setItem('key',JSON.stringify(object));
}








