// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var theRun= JSON.parse(localStorage.getItem('key'));
var runs = document.getElementById("listOfRun")
function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}

        var starting_Location = theRun._startLocation
        var end_Location = theRun._endLocation
        var start_Time = theRun._startTime
        var end_Time = theRun._endTime
        
        listHTML += "<tr> <td onmousedown=\"viewRun("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + starting_Location + " &rarr; " +end_Location;
        listHTML += "<div class=\"subtitle\">" + start_Time + ", Stops: " + end_Time +"</div></td></tr>";
        runs.innerHTML=listHTML