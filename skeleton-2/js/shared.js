// Shared code needed by all three pages.

class Run{ 
    constructor(longlatStart,longlatEnd,startTime,endTime,distance,record){ 
        this._startLocation=longlatStart; 
        this._endLocation=longlatEnd; 
        this._startTime=startTime; 
        this._endTime=endTime;
        this._distanceStartDes=distance;
        this._timeRecord=record;
    } 
     
    getStartLocation(){ 
        return this._startLocation; 
    } 
     
    getEndLocation(){ 
        return this._endLocation; 
    } 
     
    getStartTime(){ 
        return this._startTime; 
    } 
     
    getEndTime(){ 
        return this._endTime; 
    } 
    
    getDistance(){
        return this._distanceStartDes;
    }
    
    getTimeRecord(){
        return this._timeRecord;
    }
    
    getDuration(){
        return this._duration;
    }
    
    setStartLocation(newLongLatStart){ 
        this._startLocation=newLongLat; 
    } 
     
    setEndLocation(newLongLatEnd){ 
        this._endLocation=newLongLatEnd; 
    } 
     
    setStartTime(newTimeStart){ 
        this._startTime=newTimeStart; 
    } 
     
    setEndTime(newTimeEnd){ 
        this._endTime=newTimeEnd; 
    } 
    
    setDistance(newDistance){
        this._distanceStartDes=newDistance;
    }
    
    setTimeRecord(newTimeRecord){
        this._timeRecord=newTimeRecord;
    }
     
     setDuration(newDuration){
        this._duration=newDuration;
    }
} 
 






// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];








// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];
