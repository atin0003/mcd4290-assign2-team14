// Code for the View Run page.
// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

    var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
    var parsedIndex = JSON.parse(runIndex);

    if (runIndex !== null)
    {
        // If a run index was specified, show name in header bar title. This
        // is just to demonstrate navigation.  You should set the page header bar
        // title to an appropriate description of the run being displayed.
        var runNames = [ "Run A", "Run B" ];
        document.getElementById("headerBarTitle").textContent = runNames[runIndex];
    }

    
    //taking information of the corresponding run
    var theRun= JSON.parse(localStorage.getItem('key'));

    var attributeAfterStore=[]
    
    for (var i =0;i<theRun.length;i++)
    {
        for(prop in theRun[i])
        {
            attributeAfterStore.push(theRun[i][prop])
        }    
    }

    var run= new Run(attributeAfterStore[0],attributeAfterStore[1],attributeAfterStore[2],attributeAfterStore[3],attributeAfterStore[4],attributeAfterStore[5])
    console.log(run)
    
    
    //calculate and show distance
    var distances = run.getDistance() + ' m'
    var distance = document.getElementById('distance')
    distance.innerHTML += distances
    
    //calculate and show duration
    var duration = run.getTimeRecord() + ' s';
    var durations = document.getElementById('duration')
    durations.innerHTML += duration

    //calculate and show average speed
    var averageSpeed = ((Number(run.getDistance()))/(Number(run.getTimeRecord()))).toFixed(1) + ' m/s';
    var avg_Speed = document.getElementById('avgSpeed')
    avg_Speed.innerHTML += averageSpeed

    //function to delete a run
    function deleteRun()
    {
        theRun.splice(parsedIndex,1)
        localStorage.setItem('key',JSON.stringify(theRun))
    }
    
    //function which sets initial value for reattempting the run
    function reAttempt()
    {
        localStorage.setItem('index','value')
    }
    
    //marker of starting position
    var randomMarker1 = run.getStartLocation()
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker1)
                .addTo(map);


    //pop up of starting location
    var popup1 = new mapboxgl.Popup({closeOnClick: false})
    .setLngLat(randomMarker1)
    .setHTML('<h1>Start</h1>')
    .addTo(map);


    //marker on the end location
    var randomMarker2 = run.getEndLocation()
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker2)
                .addTo(map);

    
    //pop up of the end position
    var popup2 = new mapboxgl.Popup({closeOnClick: false})
    .setLngLat(randomMarker2)
    .setHTML('<h1>End</h1>')
    .addTo(map);

    //create a line connecting current location and the destination
    map.panTo(randomMarker1)
    map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [randomMarker1,randomMarker2]
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 8
        }
    }); 
      






// The following is sample code to demonstrate navigation.
// You need not use it for final app.
