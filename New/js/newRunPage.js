// Code for the Measure Run page.
var latitude
var longitude
var currentLoc
var markers = [];
var runAttribute=[]
alert('Allow tracking position and click the button on the top right of the map to locate the user before using the app')
//always updating the user's position
navigator.geolocation.watchPosition(currentLocation)

//function to calculate long and lat of user position
function currentLocation(position){
    latitude = position.coords.latitude
    longitude = position.coords.longitude
    currentLoc = [longitude,latitude]
    return currentLoc 
}

//A condition to check whether the user is reattempting a run
if(localStorage.getItem('index') === 'value')
{
    alert('Click the reattempt button to reattempt a run')
    document.getElementById("reAttempt").disabled = false
    localStorage.setItem('index','notValue')
}

//Function that will guide the app if the user is reattempting run
function reAttempt()
{
    //Takes some information of previous run from localstorage
    document.getElementById("reAttempt").disabled = true
    document.getElementById("buttonRD").disabled = true
    var theRun= JSON.parse(localStorage.getItem('key'));
    console.log(theRun)
    var attributeAfterStore=[]
    for (var i =0;i<theRun.length;i++){
        for(prop in theRun[i]){
            attributeAfterStore.push(theRun[i][prop])
            
        }    
    }

    var run= new Run(attributeAfterStore[0],attributeAfterStore[1],attributeAfterStore[2],attributeAfterStore[3],attributeAfterStore[4],attributeAfterStore[5])
    
    //pop up  and marker of previous locations
    var randomMarker1 = run.getStartLocation()
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker1)
                .addTo(map);
    
    markers.push(marker1)
    
    var popup1 = new mapboxgl.Popup({closeOnClick: false})
    .setLngLat(randomMarker1)
    .setHTML('<h1>Start</h1>')
    .addTo(map);
    
    markers.push(popup1)


    var randomMarker2 = run.getEndLocation()
    var marker2 = new mapboxgl.Marker()
                .setLngLat(randomMarker2)
                .addTo(map);
    markers.push(marker2)
    
    var popup2 = new mapboxgl.Popup({closeOnClick: false})
    .setLngLat(randomMarker2)
    .setHTML('<h1>End</h1>')
    .addTo(map);
    
    markers.push(popup2)
    
    
    var currLoc = currentLoc
    var distA = randomMarker1[0]-currLoc[0];
    var distB = randomMarker1[1]-currLoc[1]; 
    var distances = Math.sqrt(Math.pow(distA,2)+Math.pow(distB,2));
    var roundedDistances = ((distances/0.001)*150).toFixed(1)
    console.log(roundedDistances)    
    //condition where user need to be close enough to the initial location of previous run to start it
    if(roundedDistances < 20)
    {
        
        startRun(randomMarker1,randomMarker2)
        document.getElementById("stopRD").disabled = false;
    }


}


navigator.geolocation.getCurrentPosition(currentAccuracy)

//function which calculates the accuracy
function currentAccuracy(pos)
{
   
    var acc = pos.coords.accuracy.toFixed(1);
    var accc = document.getElementById('curAcc')
    accc.innerHTML += acc
    
    /*if  (acc>20)
    {
        document.getElementById("buttonRD").disabled = true;
        
    }  
    
    else
    {
        document.getElementById("buttonRD").disabled = false;
    }*/
    
}

//function which generates random destination
function randomDestination(array) {
    var destination = ['','']
    var num = (Math.random() * 0.0006) + 0.0004 ;
    var angle = Math.random() * (2*(Math.PI))
    if (angle>(1.5*Math.PI))
    {
        destination[0] = array[0] + num*Math.cos((2*(Math.PI))-angle)
        destination[1] = array[1] - num*Math.sin((2*(Math.PI))-angle)
        
    }
    else if (angle>(Math.PI))
    {
        destination[0] = array[0] - num*Math.cos(angle-(Math.PI))
        destination[1] = array[1] - num*Math.sin(angle-(Math.PI))
    }
    else if (angle>(0.5*Math.PI))
    {
        destination[0] = array[0] - num*Math.cos((Math.PI)-angle)
        destination[1] = array[1] + num*Math.sin((Math.PI)-angle)
    }
    else
    {
        destination[0] = array[0] + num*Math.cos(angle)
        destination[1] = array[1] + num*Math.sin(angle)
    }
   
    
        ;
        
    return destination
}

//function to place a marker in a random destination
function takeRandomDest()
{
   
    var randomMarker = randomDestination(currentLoc)
    var marker1 = new mapboxgl.Marker()
                .setLngLat(randomMarker)
                .addTo(map);
    markers.push(marker1)
         
    
    document.getElementById("buttonRD").disabled = true
    document.getElementById("delButtonRD").disabled = false
    document.getElementById("startRD").disabled = false
}

//function to delete a marker
function deleteRandomDest()
{
    for (var i = markers.length - 1; i >= 0; i--) 
    {
      markers[i].remove();
        
    }
    
    document.getElementById("delButtonRD").disabled = true
    document.getElementById("startRD").disabled = true
    document.getElementById("buttonRD").disabled = false
}

var counts;
//function to start a run
function startRun(startLoc,desLoc) 
{   
    
    runAttribute.push(startLoc)
    runAttribute.push(desLoc)
    var startTime=new Date()
    runAttribute.push(startTime)
    
    
    // calculate distance from user to destination which is dynamically changing
    function dynamicDistance()
    {
        var disA=desLoc[0]-startLoc[0];
        var disB=desLoc[1]-startLoc[1]; 
        var distance=Math.sqrt(Math.pow(disA,2)+Math.pow(disB,2));
        var roundedDistance = ((distance/0.001)*150).toFixed(1)
        runAttribute.push(roundedDistance) 
        var dist = document.getElementById('curDis')
        dist.innerHTML = roundedDistance
        
    }
    setInterval(dynamicDistance,1000)
    
   
    var second = 0;
    var minute = 0;
    function count() 
    { 
        second++;
        if (second === 60) 
        {
            second = 0;
            minute++;
        }
            var time = document.getElementById('Time');
            time.innerHTML = " " + minute + " " + " minute " + second + " " + "second";
    }  
    
    counts= setInterval(count,1000)
    
    document.getElementById("delButtonRD").disabled = true;
    document.getElementById("storeBut").disabled = false;
    document.getElementById("startRD").disabled = true;
    document.getElementById("buttonRD").disabled = true;
    document.getElementById("stopRD").disabled = false;
    
} 

//takes time when run is stopped and deletes the marker
function stopRun()
{
    var stopTime=new Date()
    for (var i = markers.length - 1; i >= 0; i--) 
    {
      markers[i].remove();
        
    }
    clearInterval(counts)
    return stopTime
}
var runList=[]

//store the run into an array
function storeRun()
{
    
    runAttribute.push(stopRun());
    console.log(runAttribute);
    var duration=Math.ceil((runAttribute[4]-runAttribute[2])/1000);
    console.log(duration);
    var timeStart=runAttribute[2];
    var startLoc=runAttribute[0];
    var desLoc=runAttribute[1];
    var distance=runAttribute[3];
    var stopTime=runAttribute[4];
    var run=new Run(startLoc,desLoc,timeStart,stopTime,distance,duration);
    console.log(run)
    runList.push(run)
    console.log(runList)
    document.getElementById("storeBut").disabled = false;
    document.getElementById("stopRD").disabled = true;
    document.getElementById("buttonRD").disabled = false;
    return runList
}



//store the run into localstorage
var key;
function localStore(object)
{
    localStorage.setItem('key',JSON.stringify(object));
}








